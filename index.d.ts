export * from "./fn"
export * from "./object"
export * from "./class"
export * from "./promise"
export * from "./event-emitter"
import "./globals"

/**
 * All TypeScript primitive types
 */
export type primitive = string | number | boolean | symbol | bigint

/**
 * Return the types of values of an object
 */
export type ValueOf<T> = T[keyof T]

/**
 * Allows the type to also be undefined.
 */
export type Maybe<T> = T | undefined

/**
 * Allows the type to also be null or undefined.
 */
export type Nullable<T> = T | null | undefined

/**
 * Removes `readonly` from an object.
 */
export type Mutable<T> = { -readonly [P in keyof T]: T[P] }
