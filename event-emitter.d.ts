/**
 * @example
 *
 * import { EventEmitter as TypedEventEmitter } from 'fp.d.ts'
 * import EventEmitter from 'events';
 *
 * const ee: TypedEventEmitter<{
 *    subscribe: string
 *    unsubscribe: string
 *    change: Event
 * }> = new EventEmitter();
 *
 */
export type EventEmitter<T extends object> = {
  once<K extends keyof T>(key: K, callback: (value: T[K]) => void): void;
  once(key: string, callback: (value: any) => void): void;

  on<K extends keyof T>(key: K, callback: (value: T[K]) => void): void;
  on(key: string, callback: (value: any) => void): void;

  off<K extends keyof T>(key: K, callback: (value: T[K]) => void): void;
  off(key: string, callback: (value: any) => void): void;

  emit<K extends keyof T>(key: K, callback: (value: T[K]) => void): void;
  emit(key: string, callback: (value: any) => void): void;
}
