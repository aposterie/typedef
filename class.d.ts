/**
 * Returns the type of `T`’s class constructor function.
 */
export type Class<T> = { new (...args: any[]): T }
