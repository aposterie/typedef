/**
 * Unwrap a promise
 */
export type UnwrapPromise<T> =
  T extends PromiseLike<infer K> ? K : never
