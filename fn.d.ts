//// Function Types
export type EmptyFunction = () => void
export type Consumer<T> = (value: T) => void
export type BiConsumer<T, T1> = (arg: T, arg1: T1) => void
export type TriConsumer<T, T1, T2> = (arg: T, arg1: T1, arg2: T2) => void

export type Predicate<T> = (value: T) => boolean
export type BiPredicate<T, T1> = (arg: T, arg1: T1) => boolean
export type TypePredicate<T> = (value: any) => value is T

export type Supplier<T> = () => T

export type UnaryOperator<T> = (value: T) => T
export { UnaryOperator as Endofunction }
export type BinaryOperator<T> = (left: T, right: T) => T

export type AnyFn = (...args: any[]) => any
export type Transform<T, R> = (value: T) => R
export type BiFunction<T, T1, R> = (arg: T, arg1: T1) => R
export type TriFunction<T, T1, T2, R> = (arg: T, arg1: T1, arg2: T2) => R

export type AsyncTransform<T, R> = (value: T) => Promise<R>

/**
 * Types for manipulating function parameters.
 */
export namespace Param {
  // Built-in: Parameters<typeof fn> → [arg1, arg2, ...]
  // Built-in: ReturnType<typeof fn> → T

  /**
   * Returns the `Pos`th parameter of a function.
   * @param Pos index of parameter, starting at 0
   * @param Fn type of the function
   */
  export type At<Pos extends number, Fn extends AnyFn> =
      Pos extends 0 ? (Fn extends (a: infer K) => any ? K : never)
    : Pos extends 1 ? (Fn extends (_, a: infer K) => any ? K : never)
    : Pos extends 2 ? (Fn extends (_, _1, a: infer K) => any ? K : never)
    : Pos extends 3 ? (Fn extends (_, _1, _2, a: infer K) => any ? K : never)
    : Pos extends 4 ? (Fn extends (_, _1, _2, _3, a: infer K) => any ? K : never)
    : Pos extends 5 ? (Fn extends (_, _1, _2, _3, _4, a: infer K) => any ? K : never)
    : Pos extends 6 ? (Fn extends (_, _1, _2, _3, _4, _5, a: infer K) => any ? K : never)
    : Pos extends 7 ? (Fn extends (_, _1, _2, _3, _4, _5, _6, a: infer K) => any ? K : never)
    : any

  /**
   * Get the parameters except the first `Count` ones from a function.
   * Useful for curried functions.
   */
  export type Rest<Count extends number, Fn extends AnyFn> =
      Count extends 0 ? (Fn extends (a) => any ? ArrayLike<never> : never)
    : Count extends 1 ? (Fn extends (_, ...a: infer K) => any ? K : never)
    : Count extends 2 ? (Fn extends (_, _1, ...a: infer K) => any ? K : never)
    : Count extends 3 ? (Fn extends (_, _1, _2, ...a: infer K) => any ? K : never)
    : Count extends 4 ? (Fn extends (_, _1, _2, _3, ...a: infer K) => any ? K : never)
    : Count extends 5 ? (Fn extends (_, _1, _2, _3, _4, ...a: infer K) => any ? K : never)
    : any
}
