import { AnyFn } from "./fn"

/**
 * Basic dictionary type of string keys.
 */
export type Dictionary<T> = {
  [key: string]: T
}

/**
 * Basic dictionary type of number keys.
 */
export type NumDictionary<T> = {
  [key: number]: T;
}

/**
 * An object containing at least one specific field of `[K]: V`.
 */
export type ObjectOf<K extends string | symbol | number, V> = {
  [key in K]: V
}

/**
 * Pick all fields from `T` where its value is of type `V`.
 * @example
 * 
 * type T = { a: string, b: number, c: number, d(): void }
 * type R = PickByValue<T, number>
 * R == { b: number, c: number }
 */
export type PickByValue<T extends object, V> = Pick<T, {
  [K in keyof T]: T[K] extends V ? K : never
}[keyof T]>

/**
 * Pick keys of all the functions in an object.
 * 
 * @example
 * type T = { a: string, b: number, c(): void, d(v: string): number }
 * type R = MethodKeys<T>
 * R == "c" | "d"
 */
export type FunctionKeys<T extends object> = PickByValue<T, AnyFn>
